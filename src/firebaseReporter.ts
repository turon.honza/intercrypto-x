import * as firebaseAdmin from "firebase-admin";

import { ProfitablePair } from "./priceComparator";

export const reportProfitablePair = async (line: ProfitablePair) => {
  const db = firebaseAdmin.database();
  const dbRef = db.ref("profitable-pairs");
  try {
    await dbRef.push().set(line);
  } catch (e) {
    console.error("Pushing to DB unsuccessful");
  }
};
