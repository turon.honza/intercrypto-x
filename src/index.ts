import * as firebaseAdmin from "firebase-admin";

import { ExchangeOptions, requestExchangeTicker } from "./exchangeRequest";
import { priceComparator } from "./priceComparator";
import { reportProfitablePair } from "./firebaseReporter";

type Exchanges = "binance" | "bitfinex" | "poloniex" | "kraken";

type ReqCallbackOpts = { [key in Exchanges]: ExchangeOptions };

const exchangeOpts: ReqCallbackOpts = {
  binance: {
    host: "api.binance.com",
    path: "/api/v3/depth?symbol=LTCBTC",
    fee: 0.001,
    parser: (json) => ({
      bid: parseFloat(json.bids[0][0]),
      ask: parseFloat(json.asks[0][0]),
    }),
  },
  bitfinex: {
    host: "api-pub.bitfinex.com",
    path: "/v2/ticker/tLTCBTC",
    fee: 0.002,
    parser: (json) => ({
      bid: json[0],
      ask: json[2],
    }),
  },
  poloniex: {
    host: "poloniex.com",
    path: "/public?command=returnTicker",
    fee: 0.00095,
    parser: (json) => ({
      bid: parseFloat(json.BTC_LTC.highestBid),
      ask: parseFloat(json.BTC_LTC.lowestAsk),
    }),
  },
  kraken: {
    host: "api.kraken.com",
    path: "/0/public/Ticker?pair=ltcxbt",
    fee: 0.0026,
    parser: (json) => ({
      bid: parseFloat(json.result.XLTCXXBT.b[0]),
      ask: parseFloat(json.result.XLTCXXBT.a[0]),
    }),
  },
};

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert("../firebase-key.json"),
  databaseURL: "https://intercrypto-x.firebaseio.com",
});

setInterval(async () => {
  const exchangesData = await Promise.all(
    Object.values(exchangeOpts).map(requestExchangeTicker)
  );
  const profitablePairs = priceComparator(exchangesData);

  profitablePairs.forEach(reportProfitablePair);
}, 10000);
