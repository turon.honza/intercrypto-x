import { request, RequestOptions } from "https";


type Parser = (json: any) => { bid: number; ask: number };
export type ExchangeData = {
  host: string;
  bidWithFee: number | undefined;
  askWithFee: number | undefined;
};

export interface ExchangeOptions extends RequestOptions {
  parser: Parser;
  fee: number;
}

const getBidWithFee = (bid: number, fee: number) => bid - bid * fee;
const getAskWithFee = (ask: number, fee: number) => ask + ask * fee;

/** We are returning simple JSON everytime so we don't need to have heavyweight lib like node-fetch*/
export const requestExchangeTicker = (
  options: ExchangeOptions
): Promise<ExchangeData> =>
  new Promise((resolve) => {
    request(options, (response) => {
      let data = "";

      response.on("data", (chunk) => {
        data += chunk;
      });

      response.on("end", () => {
        try {
          const jsonResponse = JSON.parse(data);
          const { bid, ask } = options.parser(jsonResponse);

          resolve({
            host: options.host,
            bidWithFee: getBidWithFee(bid, options.fee),
            askWithFee: getAskWithFee(ask, options.fee),
          });
        } catch (e) {
          resolve({
            host: options.host,
            bidWithFee: undefined,
            askWithFee: undefined,
          });
        }
      });
    }).end();
  });
