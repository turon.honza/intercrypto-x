import { ExchangeData } from "./exchangeRequest";

export interface ProfitablePair {
  timestamp: number;
  bidExchange: string;
  askExchange: string;
  percentProfitability: number;
}

/** Round-robin iteration over every exchange data */
export function priceComparator(bidAskData: ExchangeData[]): ProfitablePair[] {
  let profitablePairs: ProfitablePair[] = [];

  const timestamp = Date.now();

  bidAskData.forEach((exchangeData, index) => {
    for (let i = index + 1; i < bidAskData.length; i++) {

      // Don't report if data invalid or not profitable
      if (
        exchangeData.askWithFee &&
        bidAskData[i].bidWithFee &&
        exchangeData.askWithFee < bidAskData[i].bidWithFee
      ) {
        const percentProfitability = (exchangeData[2] / bidAskData[i][1]) * 100;

        profitablePairs.push({
          timestamp,
          bidExchange: bidAskData[i].host,
          askExchange: exchangeData.host,
          percentProfitability,
        });
      }
    }
  });
  return profitablePairs;
}
