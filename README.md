# intercrypto-x

Simple script which aggregates crypto exchanges data on LTC/BTC pair
and saves data when [exchange arbitrage](https://medium.com/@arbidexpromo/exchange-arbitrage-explained-75d87b03d420) is profitable for further analysis.

## Prerequisities

- Node.js 14
- Yarn
- Copied `firebase-key.json` from your firebase project to root folder

## Setup

Run `yarn install` in root folder

## Running

Execute `yarn build-start`
